# Youtrack Linker Plugin

Easy to use Google Chrome plugin to get a JSPWiki formatted link to the current web page.
Created and tested for Youtrack issue pages.

## How to install

* Get the unpacked extension

```
git clone https://seidrman@bitbucket.org/seidrman/wiki-linker.git
```

* Go to [chrome://extensions/](chrome://extensions/) in your *Chrome* web browser

* check the box for *Developer mode* then select *Load Unpacked*

* select the downloaded folder

## How to use

* click on extension icon
* paste :D